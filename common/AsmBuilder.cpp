
#include "AsmBuilder.h"
#include <algorithm>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <iostream>
#include <fstream>

AsmBuilder::AsmBuilder() = default;

/// Adds a vector of data to the assembly source
/// \param data
/// \return
AsmBuilder &AsmBuilder::AddData(const bytes &data) {
	for (auto row:data) {
		_lines.emplace_back(fmt::format("\tdb\t${:02x}", fmt::join(row, ",$")));
	}
	return *this;
}

/// Returns the assembly source as a string
/// \return
std::string AsmBuilder::ToString() const {
	return fmt::format("{}", fmt::join(_lines, "\n"));
}

/// Adds a label to the assembly source
/// \param name
/// \return
AsmBuilder &AsmBuilder::AddLabel(const std::string &name) {
	_lines.emplace_back(fmt::format("{}:", name));
	return *this;
}

/// Adds a comment to the assembly source
/// \param comment
/// \return
AsmBuilder &AsmBuilder::AddComment(const std::string &comment) {
	_lines.emplace_back(";---------------------------------------------------------------------------------------------------");
	_lines.emplace_back(fmt::format("; {}", comment));
	_lines.emplace_back(";---------------------------------------------------------------------------------------------------");
	return *this;
}

/// Adds a vector of data to the assembly source
/// \param data
/// \param max_width the maximum number of data items after the db directive
/// \return
AsmBuilder &AsmBuilder::AddData(const std::vector<size_t> &data, size_t max_width) {
	std::vector<size_t> block;
	auto                count = 0;
	for (auto           i: data) {
		block.emplace_back(i);
		count++;
		if (block.size() == max_width) {
			_lines.emplace_back(fmt::format("\tdb\t${:02x}", fmt::join(block, ",$")));
			block.clear();
			count = 0;
		}
	}
	return *this;
}

/// Save the assembly source to the specified file path
/// \param filepath
void AsmBuilder::Save(const std::string &filepath) const {
	std::ofstream o(filepath, std::ofstream::trunc);
	o << fmt::format("{}\n", fmt::join(_lines, "\n"));
	o.close();
}
