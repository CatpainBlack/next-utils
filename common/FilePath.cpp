#include "FilePath.h"
#include <filesystem>
#include <map>

FileType FilePath::GetFileTypeFromExtension(const std::string &filePath) {
	std::filesystem::path p(filePath);
	std::string           extension;
	if (p.has_extension()) {
		extension = p.extension().string();
		transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
	} else {
		return FileType::None;
	}

	auto itr = optionStrings.find(extension);
	if (itr != optionStrings.end()) {
		return itr->second;
	}
	return FileType::Unknown;
}

const std::map<std::string, FileType> FilePath::optionStrings = [] {
	std::map<std::string, FileType> values = {
			{".asm", FileType::Asm},
			{".s",   FileType::Asm},
			{".bin", FileType::Binary},
			{".raw", FileType::Binary},
			{".pal", FileType::Palette},
	};

	return values;
}();