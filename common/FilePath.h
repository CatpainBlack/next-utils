#pragma once

#include <string>
#include <map>

enum class FileType {
	Unknown,
	None,
	Asm,
	Palette,
	Binary,
};

class FilePath {
public:
	static FileType GetFileTypeFromExtension(const std::string &filePath);

private:
	static const std::map<std::string, FileType> optionStrings;
};


