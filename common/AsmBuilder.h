#pragma once
#include <vector>
#include <string>

class AsmBuilder {
	typedef std::vector<std::vector<uint8_t>> bytes;
public:
	AsmBuilder();
	AsmBuilder &AddData(const bytes &data);
	AsmBuilder &AddData(const std::vector<size_t> &data, size_t max_width);
	AsmBuilder &AddLabel(const std::string &name);
	AsmBuilder &AddComment(const std::string &comment);

	void Save(const std::string &filepath) const;
	[[nodiscard]] std::string ToString() const;

private:
	std::vector<std::string> _lines;
};


