#include <fmt/color.h>
#include "Options.h"
#include "BitFont.h"
#include "../common/AsmBuilder.h"

int main(int, char *argv[]) {
	Options options(argv);
	options.DisplayBanner();
	if (!options.Validate()) {
		return 1;
	}

	BitFont font(options._inputFile);
	font.SetCharHeight(8);
	if (options._verbose) {
		fmt::print("Loaded {} character glyphs\n", font.CountChars());
	}

	if (options.make_proportional) {
		if (options._verbose) {
			fmt::print("Calculating glyph widths\n");
		}
		font.MakeProportional();
	}

	AsmBuilder asmBuilder;
	asmBuilder.AddComment("Font glyphs")
			.AddLabel("font_glyphs");
	if (options.four_bit) {
		if (options._verbose) {
			fmt::print("Converting to 4bit glyphs\n");
		}
		auto dt = font.ConvertTo4bpp(options.ink, options.paper);
		if (options.interleave) {
			dt = font.ToInterleaved(dt);
		}
		asmBuilder.AddData(dt);
	} else {
		asmBuilder.AddData(font.Glyphs(), 8);
	}
	if (options.make_proportional) {
		asmBuilder.AddComment("Character width table")
				.AddLabel("font_widths")
				.AddData(font.GetWidths(), 8);
	}
	asmBuilder.Save(options._outputFile);
	if (options._verbose) {
		fmt::print("Output written to {}\n", options._outputFile);
	}
	return 0;
}

