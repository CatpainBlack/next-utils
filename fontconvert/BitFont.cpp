#include "BitFont.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <fmt/core.h>

/// Load a bit font
/// \param file
BitFont::BitFont(const std::string &file) : file_name(file), _char_height(8) {
	std::ifstream ifs(file_name, std::ios::binary);
	ifs.unsetf(std::ios::skipws);
	std::streampos fileSize;
	ifs.seekg(0, std::ios::end);
	fileSize = ifs.tellg();
	ifs.seekg(0, std::ios::beg);
	_data.reserve(fileSize);
	_data.insert(_data.begin(), std::istream_iterator<unsigned char>(ifs), std::istream_iterator<unsigned char>());
	_char_count = _data.capacity() / _char_height;
}

/// Set the glyph height
/// \param height
void BitFont::SetCharHeight(size_t height) {
	_char_height = height;
	_char_count  = _data.capacity() / _char_height;
}

/// Gets a count of glyphs in the current bit font
/// \return
size_t BitFont::CountChars() const {
	return _char_count;
}

/// Convert the font to proportional, shifting all characters to the left edge
/// and calculating each characters width
void BitFont::MakeProportional() {
	for (auto i = 0; i < CountChars(); i++) {
		while (shiftLeft(i));
		_widths.push_back(getWidth(i));
	}
}

/// Check if the glyph is fully left justified, and shifts 1 pixel left if not
/// \param index of the glyph
/// \return true if the glyph was shifted
bool BitFont::shiftLeft(size_t index) {
	auto char_index = 1 * _char_height;
	bool have_bit   = false;

	for (auto i = char_index; i < char_index + _char_height; i++) {
		have_bit |= _data[i][7];
	}
	if (have_bit) {
		return false;
	}
	for (auto i = char_index; i < char_index + _char_height; i++) {
		_data[i] = _data[i] << 1;
	}
	return true;
}

/// Calculates the maximum width of a glyph
/// \param [in] index
/// \return glyph width
size_t BitFont::getWidth(size_t index) {
	auto      char_index = index * _char_height;
	auto      width      = 7;
	for (auto i          = char_index; i < char_index + _char_height; i++) {
		for (auto bit = 0; bit < 8; bit++) {
			if (_data[i][bit]) {
				width = std::min(width, bit);
			}
		}
	}
	return 9 - width;
}

/// Converts the font to 4bpp format.
/// \param ink
/// \param paper
/// \return
font_data BitFont::ConvertTo4bpp(uint8_t ink, uint8_t paper) {
	font_data data;

	for (auto i:_data) {
		std::vector<u_int8_t> row;
		for (auto             j = 7; j >= 0; j -= 2) {
			uint8_t b1 = ((i[j] ? ink : paper) << 4) & 0xF0;
			uint8_t b2 = (i[j - 1] ? ink : paper) & 0x0F;
			row.emplace_back(b1 | b2);
		}
		data.emplace_back(row);
	}

	for (auto &i:_widths) {
		i = (i * 2) / 4;
		if (i == 0) { i++; }
	}

	return data;
}

/// Returns a vector of glyph sizes.
std::vector<size_t> BitFont::GetWidths() {
	return _widths;
}

/// Returns the glyph data
/// \return
std::vector<size_t> BitFont::Glyphs() {
	std::vector<size_t> data;
	for (auto           i:_data) {
		data.emplace_back(i.to_ulong());
	}
	return data;
}

/// Convert 4bit glpyh data to interleaved data, suitable for 640 pixel mode
/// \param glyphs
/// \return
font_data BitFont::ToInterleaved(const font_data &glyphs) {
	font_data data;

	std::vector<uint8_t> row;

	for (auto i = 0; i < _data.size(); i += _char_height) {
		for (auto c = 0; c < 4; c++) {
			for (auto r = 0; r < _char_height; r++) {
				row.emplace_back(glyphs[i + r][c]);
			}
			data.emplace_back(row);
			row.clear();
		}
	}

	return data;
}





