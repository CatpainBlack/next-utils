#pragma once
#include <string>
#include <bitset>
#include <vector>

typedef std::vector<std::vector<uint8_t>> font_data;

class BitFont {
public:
	explicit BitFont(const std::string &file);
	void SetCharHeight(size_t height);
	[[nodiscard]] size_t CountChars() const;
	void MakeProportional();
	font_data ConvertTo4bpp(uint8_t ink, uint8_t paper);
	font_data ToInterleaved(const font_data &glyphs);
	std::vector<size_t> Glyphs();
	std::vector<size_t> GetWidths();

protected:
	bool shiftLeft(size_t index);
	size_t getWidth(size_t index);

private:
	std::vector<std::bitset<8>> _data;
	std::vector<size_t>         _widths;
	size_t                      _char_height;
	size_t                      _char_count;
	const std::string           &file_name;
};


