#pragma once

#include <string>
#include <argh.h>

class Options {
public:
	explicit Options(char **argv);
	void DisplayBanner() const;
	[[nodiscard]] bool Validate() const;
	void ShowUsage();

public:
	bool         _verbose, _suppressBanner, make_proportional, four_bit, interleave, masked;
	unsigned int ink, paper;
	std::string  _inputFile;
	std::string  _outputFile;
};

