#include <fmt/color.h>
#include <filesystem>
#include "Options.h"
#include "../png2next/Options.h"

/// Parse the command line parameters
/// \param argv
Options::Options(char **argv) : _verbose(false), four_bit(false), interleave(false), masked(false) {
	argh::parser cmd(argv);
	if (cmd[{"-h", "--help"}]) {
		DisplayBanner();
		ShowUsage();
		return;
	}
	_verbose          = cmd[{"-v", "--verbose"}];
	_suppressBanner   = cmd[{"-sb", "--suppress-banner"}];
	make_proportional = cmd[{"-p", "--proportional"}];
	four_bit          = cmd[{"-4", "--4bit"}];
	interleave        = cmd[{"-l", "--interleave"}];
	masked            = cmd[{"-m", "--masked"}];
	cmd({"-i", "--ink"}, 1u) >> ink;
	cmd({"-p", "--paper"}, 0u) >> paper;
	_inputFile  = cmd[1];
	_outputFile = cmd[2];
}

/// Performs command line parameter validation
/// \return true if the options are valid
bool Options::Validate() const {
	if (_inputFile.empty()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "No input file specified.");
		return false;
	}
	if (_outputFile.empty()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "No output file specified.");
		return false;
	}
	if (!std::filesystem::exists(_inputFile)) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "SourceFilePath file is not accessible.");
		return false;
	}
	return true;
}

/// Displays the program DisplayBanner
void Options::DisplayBanner() const {
	if (!_suppressBanner) {
		fmt::print("Font Convert - Convert 8bit fonts to next bitmap formats.\n");
		fmt::print("(c)2021 Captain Black/Twisted Raven.\n");
	}
}

/// Show commandline usage
void Options::ShowUsage() {
	fmt::print(""
			   "fontconvert <input> <output> [options]\n"
			   "Options:\n"
			   "\t-h,--help\t\tDisplay usage and options\n"
			   "\t-v,--verbose\tDisplay detailed information\n"
			   "\t-sb,--suppress-banner\tDon't display copyright information\n"
			   "\t-p,--proportional\tConvert to proportional font\n"
			   "\t-l,--interleave\t\tArrange pixels for 640x256 mode\n"
			   "\t-m,--masked\t\tInclude bit mask\n"
			   "\t-4,--4bit\t\tConvert to 4 bits per pixel (16 colours)\n"
			   "\t-i,--ink\t\tColour index to use for set pixels (1)\n"
			   "\t-p,--paper\t\tColour index to use for unset pixes (0)\n");
}






