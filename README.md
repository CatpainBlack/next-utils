# Captain Black's Next Utils

A collection of command line utitlities created to aid with my ZX Spectrum Next development.

## Bitmap Font Converter

Convert an eight bit font into different display formats.
For now, this only supports 8x8 1 bit font files.
```
fontconvert <input> <output> [options]
Options:
  -h,--help             Display usage and options
  -v,--verbose          Display detailed information
  -sb,--suppress-banner Don't display copyright information
  -p,--proportional     Convert to proportional font
  -l,--interleave       Arrange pixels for 640x256 mode
  -m,--masked           Include bit mask
  -4,--4bit             Convert to 4 bits per pixel (16 colours)
  -i,--ink              Colour index to use for set pixels (1)
  -p,--paper            Colour index to use for unset pixels (0)
```

## Png2Next

Convert PNG images to raw pixel data and palette information

Using the `--palette=<name> ` switch you may specifiy a file name to save the palette information. The extension determines how the file is created. .asm will produce a z80 assembly source file, any other will create a raw dump of the 9-bit palette data.

```
png2next <input> [options]
Options:
  -h,--help           Display usage and options.
  -v,--verbose        Display detailed information.
  -p,--palette=<file>	Saves the palette to a separate file.
  -o,--output=<file>  Specify the file to store the bitmap data.
```

