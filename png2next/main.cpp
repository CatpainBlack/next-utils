#include <fmt/color.h>
#include <AsmBuilder.h>
#include <fstream>
#include "Options.h"
#include "PngConvert.h"
#include "Palette.h"

void save_palette(const Options &options, const PngConvert &png) {
	Palette pal(png.PaletteData(), png.ColourCount());
	auto    palette = pal.As9BitPalette();
	switch (options.PaletteType()) {
		case FileType::Asm: {
			if (options.Verbose()) {
				fmt::print(fg(fmt::color::yellow), "Generating palette assembly file: {}\n", options.PaletteFilePath());
			}
			AsmBuilder b;
			b.AddData(palette, 16);
			b.Save(options.PaletteFilePath());
			break;
		}
		case FileType::Palette: {
			if (options.Verbose()) {
				fmt::print(fg(fmt::color::yellow), "Saving palette data: {}\n", options.PaletteFilePath());
			}
			std::ofstream bin;
			bin.open(options.PaletteFilePath(), std::ios::binary | std::ios::trunc);
			std::copy(palette.begin(), palette.end(), std::ostreambuf_iterator<char>(bin));
			break;
		}
		default:
			break;
	}
}

bool save_pixels(const Options &options, PngConvert &png) {
	std::vector<uint8_t> pixels;
	switch (png.ImageWidth()) {
		case 320: {
			if (options.Verbose()) {
				fmt::print(fg(fmt::color::yellow), "Ordering pixels by column\n");
			}
			png.TopToBottomPixels(pixels);
			break;
		}
		case 256: {
			if (options.Verbose()) {
				fmt::print(fg(fmt::color::yellow), "Ordering pixels by row\n");
			}
			png.LeftToRightPixels(pixels);
			break;
		}
		default:
			return false;
	}

	switch (options.ImageType()) {
		case FileType::Binary: {
			if (options.Verbose()) {
				fmt::print(fg(fmt::color::yellow), "Saving image data: {}\n", options.ImageFilePath());
			}
			std::ofstream bin;
			bin.open(options.ImageFilePath(), std::ios::binary | std::ios::trunc);
			std::copy(pixels.begin(), pixels.end(), std::ostreambuf_iterator<char>(bin));
			break;
		}
		default:
			fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Unsupported output format");
			return false;
	}
	return true;
}

int main(int, char *argv[]) {

	Options options(argv);
	if (!options.Validate()) {
		return 1;
	}

	PngConvert png(options.SourceFilePath());

	if (0 != png.Decode(false)) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Decode error {} \n", png.ErrorText());
		return 1;
	}

	if (!png.IsEightBitIndexed()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Not an 8 bit indexed png\n");
		return 2;
	}

	save_palette(options, png);
	if (options.ImageType() == FileType::Binary) {
		if (!save_pixels(options, png)) {
			fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Could not export pixel data\n");
		}
	}

	return 0;
}
