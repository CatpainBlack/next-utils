#include <utility>
#include "PngConvert.h"

PngConvert::PngConvert(std::string filePath) : _filePath(std::move(filePath)),
											   _w(-1), _h(-1), _last_error(0) {
}

unsigned PngConvert::Decode(bool colourConvert) {
	_state.decoder.color_convert = colourConvert;
	_last_error = lodepng::load_file(_buffer, _filePath);
	if (_last_error) {
		return _last_error;
	}
	return lodepng::decode(_image, _w, _h, _state, _buffer);
}

std::string PngConvert::ErrorText() const {
	return lodepng_error_text(_last_error);
}

bool PngConvert::IsEightBitIndexed() const {
	return _state.info_raw.colortype == LCT_PALETTE
		   && _state.info_raw.bitdepth == 8;
}

unsigned char *PngConvert::PaletteData() const {
	return _state.info_raw.palette;
}

size_t PngConvert::ColourCount() const {
	return _state.info_raw.palettesize;
}

void PngConvert::TopToBottomPixels(std::vector<uint8_t> &pixels) {
	for (auto r = 0; r < 320; r++) {
		for (auto c = 0; c < 256; c++) {
			pixels.push_back(_image[c + (r * 256)]);
		}
	}
}

void PngConvert::LeftToRightPixels(std::vector<uint8_t> &pixels) {
	std::vector<uint8_t> p;
	for (auto            r = 0; r < 192; r++) {
		for (auto c = 0; c < 256; c++) {
			pixels.push_back(_image[c + (r * 192)]);
		}
	}
}

size_t PngConvert::ImageWidth() const {
	return _w;
}
