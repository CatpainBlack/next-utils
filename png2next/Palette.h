#pragma once
#include <vector>

typedef struct {
	uint8_t red, green, blue;
} Rgb;

class Palette {
public:
	explicit Palette(const unsigned char *palette, size_t count);
	[[nodiscard]] std::vector<size_t> As9BitPalette() const;

private:
	std::vector<Rgb>              _colours;

	static Rgb NearestColour(const Rgb &rgb);
	static const std::vector<int> _nextColours;
};

