#include "Palette.h"
#include <vector>

const std::vector<int> Palette::_nextColours = [] {
	std::vector<int> values;
	values.push_back(0);
	for (auto b = 16; b < 255; b += 16) {
		values.push_back(b);
	}
	values.push_back(255);
	return values;
}();

Palette::Palette(const unsigned char *palette, size_t count) {
	_colours.resize(256);
	auto      pi = 0;
	for (auto i  = 0; i < 256; i++) {
		if (i >= count) {
			_colours[i] = {0, 0, 0};
			continue;
		}
		auto r1 = palette[pi++];
		auto g1 = palette[pi++];
		auto b1 = palette[pi++];
		_colours[i] = NearestColour({r1, g1, b1});
	}
}

Rgb Palette::NearestColour(const Rgb &rgb) {
	auto r1 = _nextColours[static_cast<uint8_t>(ceil(rgb.red / 16))];
	auto g1 = _nextColours[static_cast<uint8_t>(ceil(rgb.green / 16))];
	auto b1 = _nextColours[static_cast<uint8_t>(ceil(rgb.blue / 16))];
	auto r2 = _nextColours[static_cast<uint8_t>(floor(rgb.red / 16))];
	auto g2 = _nextColours[static_cast<uint8_t>(floor(rgb.green / 16))];
	auto b2 = _nextColours[static_cast<uint8_t>(floor(rgb.blue / 16))];
	auto d1 = sqrt(sqrt(r1 - rgb.red) + sqrt(g1 - rgb.green) + sqrt(b1 - rgb.blue));
	auto d2 = sqrt(sqrt(r2 - rgb.red) + sqrt(g2 - rgb.green) + sqrt(b2 - rgb.blue));
	if (d2 > d1) {
		return {static_cast<uint8_t>(r1), static_cast<uint8_t>(g1), static_cast<uint8_t>(b1)};
	}
	return {static_cast<uint8_t>(r2), static_cast<uint8_t>(g2), static_cast<uint8_t>(b2)};
}

std::vector<size_t> Palette::As9BitPalette() const {
	std::vector<size_t> entries;
	for (auto           &p:_colours) {
		auto r  = p.red >> 5u;
		auto g  = p.green >> 5u;
		auto b  = p.blue >> 5u;
		auto bh = ((r << 5u) | (g << 2u) | b >> 1u);
		auto bl = (b & 1u); // (priority ? 128 : 0);
		entries.push_back(bh);
		entries.push_back(bl);
	}
	return entries;
}
