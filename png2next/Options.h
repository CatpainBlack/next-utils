#pragma once

#include "FilePath.h"

class Options {
public:
	explicit Options(char **argv);
	[[nodiscard]] bool Validate();
	[[nodiscard]] FileType PaletteType() const;
	[[nodiscard]] FileType ImageType() const;
	[[nodiscard]] std::string SourceFilePath() const;
	[[nodiscard]] std::string PaletteFilePath() const;
	[[nodiscard]] std::string ImageFilePath() const;
	[[nodiscard]] bool Verbose() const;

private:
	void DisplayBanner() const;
	static void ShowUsage();

	bool validatePaletteFileName();
	bool validateImageFileName();

private:
	bool        _verbose, _suppressBanner, _savePalette, _saveImage;
	FileType    _paletteType, _imageType;
	std::string _inputFile;
	std::string _paletteFilePath;
	std::string _imageFilePath;
};

