#include <argh.h>
#include <fmt/color.h>
#include <filesystem>
#include <FilePath.h>
#include "Options.h"

Options::Options(char **argv) : _verbose(false), _suppressBanner(false), _savePalette(false) {
	argh::parser cmd(argv);
	if (cmd[{"-h", "--help"}]) {
		DisplayBanner();
		ShowUsage();
		return;
	}
	_verbose         = cmd[{"-v", "--verbose"}];
	_suppressBanner  = cmd[{"-sb", "--suppress-banner"}];
	_paletteFilePath = cmd({"-p", "--palette"}).str();
	_imageFilePath   = cmd({"-o", "--output"}).str();
	_inputFile       = cmd[1];
}

void Options::DisplayBanner() const {
	if (!_suppressBanner) {
		fmt::print("png2next - Convert png images to Next native bitmaps.\n");
		fmt::print("(c)2021 Captain Black/Twisted Raven.\n");
	}
}

bool Options::Validate() {
	if (_verbose) DisplayBanner();

	if (_inputFile.empty()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "No input file specified.");
		return false;
	}

	if (!std::filesystem::exists(_inputFile)) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "SourceFilePath file is not accessible.");
		return false;
	}
	if (!validatePaletteFileName()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Cannot determine palette file type.");
		return false;
	}

	if (!validateImageFileName()) {
		fmt::print(fg(fmt::color::crimson) | fmt::emphasis::bold, "Cannot determine output image file type.");
		return false;
	}

	return true;
}

bool Options::validateImageFileName() {
	if (_imageFilePath.empty()) {
		_saveImage = false;
		_imageType = FileType::None;
		return true;
	}
	_imageType = FilePath::GetFileTypeFromExtension(_imageFilePath);
	_saveImage = (_imageType != FileType::None || _imageType != FileType::Unknown);
	return _saveImage;
}

bool Options::validatePaletteFileName() {
	if (_paletteFilePath.empty()) {
		_savePalette = false;
		_paletteType = FileType::None;
		return true;
	}
	_paletteType = FilePath::GetFileTypeFromExtension(_paletteFilePath);
	_savePalette = (_paletteType == FileType::Asm || _paletteType == FileType::Palette);
	return _savePalette;
}

std::string Options::SourceFilePath() const {
	return _inputFile;
}

FileType Options::PaletteType() const {
	return _paletteType;
}

std::string Options::PaletteFilePath() const {
	return _paletteFilePath;
}

bool Options::Verbose() const {
	return _verbose;
}

FileType Options::ImageType() const {
	return _imageType;
}

std::string Options::ImageFilePath() const {
	return _imageFilePath;
}

void Options::ShowUsage() {
	fmt::print(""
			   "png2next <input> [options]\n"
			   "Options:\n"
			   "\t-h,--help           Display usage and options.\n"
			   "\t-v,--verbose        Display detailed information.\n"
			   "\t-p,--palette=<file> Saves the palette to a separate file.\n"
			   "\t-o,--output=<file>  Specify the file to store the bitmap data.\n"
	);
}
