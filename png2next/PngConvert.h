#pragma once

#include <string>
#include <vector>
#include "lodepng.h"

class PngConvert {
public:
	explicit PngConvert(std::string filePath);
	unsigned Decode(bool colourConvert);
	[[nodiscard]] std::string ErrorText() const;
	[[nodiscard]] bool IsEightBitIndexed() const;
	[[nodiscard]] unsigned char *PaletteData() const;
	[[nodiscard]] size_t ColourCount() const;
	[[nodiscard]] size_t ImageWidth() const;

	void LeftToRightPixels(std::vector<uint8_t> &pixels);
	void TopToBottomPixels(std::vector<uint8_t> &pixels);

private:
	std::string                _filePath;
	lodepng::State             _state;
	std::vector<unsigned char> _buffer;
	std::vector<unsigned char> _image;
	unsigned                   _w, _h;
	unsigned                   _last_error;

};


